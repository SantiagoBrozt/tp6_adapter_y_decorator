package SantiagoBrozt.ejercicio1.main;

import SantiagoBrozt.ejercicio1.modelo.AdapterMotorElectrico;
import SantiagoBrozt.ejercicio1.modelo.Motor;
import SantiagoBrozt.ejercicio1.modelo.MotorElectrico;

public class Main {
    public static void main(String[] args) {
        Motor motorElectrico = new AdapterMotorElectrico(new MotorElectrico());
        System.out.println(motorElectrico.arrancar());
        System.out.println(motorElectrico.acelerar());
        System.out.println(motorElectrico.apagar());
    }
}
