package SantiagoBrozt.ejercicio1.modelo;

public class MotorElectrico {

    public String conectar() {
        return "se conecta";
    }

    public String activar() {
        return "se activa";
    }

    public String moverMasRapido() {
        return "acelera";
    }

    public String detener() {
        return "se detiene";
    }

    public String desconectar() {
        return "se desconecta";
    }
}
