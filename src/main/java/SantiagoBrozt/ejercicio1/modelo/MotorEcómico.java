package SantiagoBrozt.ejercicio1.modelo;

public class MotorEcómico extends Motor {

    public String arrancar() {
        return ("arranca...");
    }

    public String acelerar() {
        return ("acelera...");
    }

    public String apagar() {
        return ("se detiene...");
    }
}
