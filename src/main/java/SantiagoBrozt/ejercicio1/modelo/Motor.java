package SantiagoBrozt.ejercicio1.modelo;

public abstract class Motor {
    public abstract String arrancar();
    public abstract String acelerar();
    public abstract String apagar();
}
