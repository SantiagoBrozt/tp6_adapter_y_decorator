package SantiagoBrozt.ejercicio1.modelo;

public class AdapterMotorElectrico extends Motor {
    private MotorElectrico motorElectrico;

    public AdapterMotorElectrico(MotorElectrico motorElectrico) {
        this.motorElectrico = motorElectrico;
    }

    @Override
    public String arrancar() {
        return motorElectrico.conectar() + " y " + motorElectrico.activar();
    }

    @Override
    public String acelerar() {
        return motorElectrico.moverMasRapido();
    }

    @Override
    public String apagar() {
        return motorElectrico.detener() + " y " + motorElectrico.desconectar();
    }
}
