package SantiagoBrozt.ejercicio2.modelo;

interface Figura {
    void dibujar(AdapterGraphics2D adapterG2d);
}
