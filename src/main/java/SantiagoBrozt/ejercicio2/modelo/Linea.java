package SantiagoBrozt.ejercicio2.modelo;

public class Linea implements Figura {
    private final Coordenada coordenada;
    private int longitud;

    public Linea(Coordenada coordenada, int longitud) {
        this.longitud = longitud;
        this.coordenada = coordenada;
    }

    @Override
    public void dibujar(AdapterGraphics2D adapterG2d) {
       adapterG2d.dibujarLInea(coordenada, longitud);
    }
}
