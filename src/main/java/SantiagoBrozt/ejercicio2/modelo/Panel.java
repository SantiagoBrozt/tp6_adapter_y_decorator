package SantiagoBrozt.ejercicio2.modelo;

public interface Panel {
    void dibujarCirculo(Coordenada coordenada, int radio);
    void dibujarLInea(Coordenada coordenada, int longitud);
    void escribirTexto(Coordenada coordenada, String texto);
}
