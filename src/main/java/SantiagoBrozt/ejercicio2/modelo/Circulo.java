package SantiagoBrozt.ejercicio2.modelo;

public class Circulo implements Figura {
    private int radio;
    private Coordenada coordenada;

    public Circulo(Coordenada coordenada, int radio) {
        this.radio = radio;
        this.coordenada = coordenada;
    }

    @Override
    public void dibujar(AdapterGraphics2D adapterG2d) {
        adapterG2d.dibujarCirculo(coordenada, radio);
    }
}
