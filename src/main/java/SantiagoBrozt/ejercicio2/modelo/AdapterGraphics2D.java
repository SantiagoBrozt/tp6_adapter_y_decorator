package SantiagoBrozt.ejercicio2.modelo;

import java.awt.*;

public class AdapterGraphics2D implements Panel {
    private Graphics2D g2d;

    public AdapterGraphics2D(Graphics2D graphics2D) {
        this.g2d = graphics2D;
    }

    @Override
    public void dibujarCirculo(Coordenada coordenada, int radio) {
        g2d.drawOval(coordenada.x() - radio,
                coordenada.y() - radio,
                radio * 2,
                radio * 2);
    }

    @Override
    public void dibujarLInea(Coordenada coordenada, int longitud) {
        g2d.drawLine(coordenada.x(), coordenada.y(),
                coordenada.x() + longitud, coordenada.y());
    }

    @Override
    public void escribirTexto(Coordenada coordenada, String texto) {
        g2d.drawString(texto, coordenada.x(), coordenada.y());
    }

}
