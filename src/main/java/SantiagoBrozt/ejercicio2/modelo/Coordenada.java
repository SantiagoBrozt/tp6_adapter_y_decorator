package SantiagoBrozt.ejercicio2.modelo;

public record Coordenada(int x, int y) {
}
