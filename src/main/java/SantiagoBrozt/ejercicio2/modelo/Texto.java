package SantiagoBrozt.ejercicio2.modelo;

public class Texto implements Figura {
    private final Coordenada coordenada;
    private String texto;

    public Texto(Coordenada coordenada, String texto) {
        this.texto = texto;
        this.coordenada = coordenada;
    }

    @Override
    public void dibujar(AdapterGraphics2D adapterG2d) {
       adapterG2d.escribirTexto(coordenada, texto);
    }
}
