package SantiagoBrozt.ejercicio3.main;

import SantiagoBrozt.ejercicio3.modelo.ConVerificacionExistencia;
import SantiagoBrozt.ejercicio3.modelo.ConVerificacionNull;
import SantiagoBrozt.ejercicio3.modelo.Reporte;
import SantiagoBrozt.ejercicio3.modelo.ReporteSimple;

import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        Reporte reporte = new ConVerificacionNull(new ReporteSimple("Hola mundo!"));
        reporte.export(new File("C:\\Users\\Sancho\\Documents\\reporte.txt"));
    }
}
