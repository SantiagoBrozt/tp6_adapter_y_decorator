package SantiagoBrozt.ejercicio3.modelo;

import java.io.File;
import java.io.IOException;

public class ConVerificacionNull implements Reporte {
    private Reporte reporte;

    public ConVerificacionNull(Reporte reporte) {
        this.reporte = reporte;
    }

    @Override
    public void export(File file) throws IOException {
        if (reporte == null) {
            throw new IllegalArgumentException(
                    "El reporte no puede ser NULL..."
            );
        }
        this.reporte.export(file);
    }
}
