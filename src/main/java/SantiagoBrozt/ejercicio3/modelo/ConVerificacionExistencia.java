package SantiagoBrozt.ejercicio3.modelo;

import java.io.File;
import java.io.IOException;

public class ConVerificacionExistencia implements Reporte{
    private Reporte reporte;

    public ConVerificacionExistencia(Reporte reporte) {
        this.reporte = reporte;
    }

    @Override
    public void export(File file) throws IOException {
        if (file.exists()) {
            throw new IllegalArgumentException(
                    "El archivo ya existe..."
            );
        }
        this.reporte.export(file);
    }


}
