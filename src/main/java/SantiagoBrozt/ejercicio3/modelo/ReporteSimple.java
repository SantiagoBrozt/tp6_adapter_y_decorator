package SantiagoBrozt.ejercicio3.modelo;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class ReporteSimple implements Reporte {
    private String reporte;

    public ReporteSimple(String reporte) {
        this.reporte = reporte;
    }

    @Override
    public void export(File file) throws IOException {
//        if (file == null) {
//            throw new IllegalArgumentException(
//                    "File es NULL; no puedo exportar..."
//            );
//        }
//        if (file.exists()) {
//            throw new IllegalArgumentException(
//                    "El archivo ya existe..."
//            );
//        }
        Files.write(file.toPath(), this.reporte.getBytes());
    }
}
