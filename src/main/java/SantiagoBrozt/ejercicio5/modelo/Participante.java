package SantiagoBrozt.ejercicio5.modelo;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Objects;

class Participante {

    private String nombre;
    private String apellido;
    private int dni;
    private int puntos;
    private RegistroDeInscripcion registro;

    Participante(String nombre, String apellido, int dni) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.dni = dni;
        this.puntos = 0;
    }
    int getPuntos() {
        return this.puntos;
    }

    boolean esTuDni(int dni){
        boolean es = false;
        if (this.dni == dni) {
            es = true;
        }
        return es;
    }
    void agregarPuntos(int puntos) {
        this.puntos += puntos;
    }
}


