package SantiagoBrozt.ejercicio5.modelo;

public interface Inscripcion {
    public void inscribirParticipante(String nombreParticipante, String apellido, int dni);
}
