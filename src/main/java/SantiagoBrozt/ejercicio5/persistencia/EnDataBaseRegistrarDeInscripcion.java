package SantiagoBrozt.ejercicio5.persistencia;

import SantiagoBrozt.ejercicio5.modelo.RegistroDeInscripcion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;

public class EnDataBaseRegistrarDeInscripcion implements RegistroDeInscripcion {

    private final static String conexion = "jdbc:mysql://localhost:3306/tp2_ejercicio_1";
    private final static String usuario= "SantiagoBrozt";
    private final static String clave = "okQFuK8ohJKeeeiK";

    @Override
    public void registrarInscripcion(LocalDate fecha, String nombreConcurso, String nombreParticipante, String apellido, int dni,
                                     int puntos) {
        String insertarParticipante = "INSERT INTO participante (nombre, apellido, dni, puntos, nombre_concurso) "
                + "VALUES (?, ?, ?, ?, ?)";
        try (Connection myConexion = DriverManager.getConnection(conexion, usuario, clave);
             PreparedStatement statementParticipante = myConexion.prepareStatement(insertarParticipante)) {
            //Insertar participante
            statementParticipante.setString(1, nombreParticipante);
            statementParticipante.setString(2, apellido);
            statementParticipante.setInt(3, dni);
            statementParticipante.setInt(4, puntos);
            statementParticipante.setString(5, nombreConcurso);
            statementParticipante.executeUpdate();
        }
        catch (SQLException e) {
            throw new RuntimeException("Error de conexion", e);
        }

    }
}

