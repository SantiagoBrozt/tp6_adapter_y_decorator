package SantiagoBrozt.ejercicio5.persistencia;


import SantiagoBrozt.ejercicio5.modelo.RegistroDeInscripcion;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;

public class EnDiscoRegistrarDeInscripcion implements RegistroDeInscripcion {

    private String localPath;

    public EnDiscoRegistrarDeInscripcion(String path) {
        this.localPath = path;
    }
    @Override
    public void registrarInscripcion(LocalDate fecha, String nombreConcurso, String nombreParticipante, String apellido, int dni,
                                     int puntos) {
        String registro = fecha + ", " + nombreParticipante + " " + apellido + ", "
                + nombreConcurso + "\n";
        try {
            Files.write(Paths.get(this.localPath),
                    registro.getBytes(),
                    StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        }
        catch (IOException e) {
            throw new RuntimeException("no se ha podido persistir", e);
        }
    }
}
