package SantiagoBrozt.ejercicio5.comunicacion;

import java.util.Properties;

import SantiagoBrozt.ejercicio5.modelo.Inscripcion;
import jakarta.mail.Message;
import jakarta.mail.MessagingException;
import jakarta.mail.PasswordAuthentication;
import jakarta.mail.Session;
import jakarta.mail.Transport;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;

public class PorEmailComunicarInscripcion implements Inscripcion{
    private Inscripcion inscripcion;
    private String mensaje;

    public PorEmailComunicarInscripcion(Inscripcion inscripcion, String mensaje) {
        this.inscripcion = inscripcion;
        this.mensaje = mensaje;
    }

    @Override
    public void inscribirParticipante(String nombreParticipante, String apellido, int dni) {
        this.inscripcion.inscribirParticipante(nombreParticipante, apellido, dni);
        //provide recipient's email ID
        String to = "sandbox.smtp.mailtrap.io";
        //provide sender's email ID
        String from = "jakartafrom@example.com";
        //provide Mailtrap's username
        final String username = "0dcc8656724b2b";
        //provide Mailtrap's password
        final String password = "0cfe0bd41c67db";
        //provide Mailtrap's host address
        String host = "smtp.mailtrap.io";
        //configure Mailtrap's SMTP server details
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", "2525");
        //create the Session object
        Session session = Session.getInstance(props,
                new jakarta.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
        try {
            //create a MimeMessage object
            Message message = new MimeMessage(session);
            //set From email field
            message.setFrom(new InternetAddress(from));
            //set To email field
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
            //set email subject field
            message.setSubject("Recibiste un mail capo!");
            //set the content of the email message
            message.setText(mensaje);
            //send the email message
            Transport.send(message);
            System.out.println("Se envió carajo");
        }
        catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
}

