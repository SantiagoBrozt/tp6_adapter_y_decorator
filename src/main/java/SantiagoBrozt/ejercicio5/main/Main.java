package SantiagoBrozt.ejercicio5.main;

import SantiagoBrozt.ejercicio5.comunicacion.PorEmailComunicarInscripcion;
import SantiagoBrozt.ejercicio5.modelo.Concurso;
import SantiagoBrozt.ejercicio5.modelo.Inscripcion;
import SantiagoBrozt.ejercicio5.persistencia.EnDiscoRegistrarDeInscripcion;

import java.time.LocalDate;
import java.util.function.Supplier;

public class Main {
    public static void main(String[] args) {
        var proveedorDeFecha = new Supplier<LocalDate>() {
            @Override public LocalDate get() {
                return LocalDate.now();
            }
        };
        var fechaActual = proveedorDeFecha.get();
        Inscripcion c = new PorEmailComunicarInscripcion(new Concurso("Concurso de ayer", fechaActual.minusDays(1), fechaActual.plusDays(1),
                new EnDiscoRegistrarDeInscripcion("C:\\Users\\Sancho\\Documents\\Inscriptos.txt"), proveedorDeFecha), "tu inscripción en el concurso fue exitosa");
        c.inscribirParticipante("Santiago", "Brozt", 42234317);

    }
}
