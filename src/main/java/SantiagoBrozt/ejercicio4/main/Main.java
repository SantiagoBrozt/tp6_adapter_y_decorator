package SantiagoBrozt.ejercicio4.main;

import SantiagoBrozt.ejercicio4.modelo.*;

public class Main {
    public static void main(String[] args) {
        Pedido p = new ConQueso(new ConTomate(new ConCarne(new ConPapas(new Combo(Combo.COMBO_BASICO)))));
        System.out.println(p.precio());
        System.out.println(p.descripcion());
    }
}
