package SantiagoBrozt.ejercicio4.modelo;

public class ConPapas implements Pedido{
    private static final int PRECIO = 200;
    private Pedido pedido;

    public ConPapas(Pedido pedido) {
        this.pedido = pedido;
    }

    @Override
    public int precio() {
        return PRECIO + pedido.precio();
    }

    @Override
    public String descripcion() {
        return pedido.descripcion() + ", con papas";
    }
}
