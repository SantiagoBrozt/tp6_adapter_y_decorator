package SantiagoBrozt.ejercicio4.modelo;

public interface Pedido {
    public int precio();
    public String descripcion();
}
