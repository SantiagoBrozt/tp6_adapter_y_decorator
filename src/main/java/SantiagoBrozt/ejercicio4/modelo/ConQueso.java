package SantiagoBrozt.ejercicio4.modelo;

public class ConQueso implements Pedido{
    private static final int PRECIO = 400;
    private Pedido pedido;

    public ConQueso(Pedido pedido) {
        this.pedido = pedido;
    }

    @Override
    public int precio() {
        return PRECIO + pedido.precio();
    }

    @Override
    public String descripcion() {
        return pedido.descripcion() + ", con Queso";
    }
}
