package SantiagoBrozt.ejercicio4.modelo;

public class Combo implements Pedido{
    public static final int COMBO_BASICO = 0;
    public static final int COMBO_FAMILIAR = 1;
    public static final int COMBO_ESPECIAL = 2;
    private int tipo;
    private int precio;
    private String descripcion;

    public Combo(int tipo) {
        this.tipo = tipo;

        if(this.tipo == COMBO_BASICO) {
            this.precio = 1000;
            this.descripcion = "Combo básico";
        }

        if(this.tipo == COMBO_FAMILIAR) {
            this.precio = 2000;
            this.descripcion = "Combo familiar";
        }

        if(this.tipo == COMBO_ESPECIAL) {
            this.precio = 3000;
            this.descripcion = "Combo especial";
        }
    }

    @Override
    public int precio() {
        return this.precio;
    }

    @Override
    public String descripcion() {
        return this.descripcion;
    }
}
