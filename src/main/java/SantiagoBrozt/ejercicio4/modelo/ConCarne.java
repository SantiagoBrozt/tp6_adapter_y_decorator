package SantiagoBrozt.ejercicio4.modelo;

public class ConCarne implements Pedido{
    private static final int PRECIO = 300;
    private Pedido pedido;

    public ConCarne(Pedido pedido) {
        this.pedido = pedido;
    }

    @Override
    public int precio() {
        return PRECIO + pedido.precio();
    }

    @Override
    public String descripcion() {
        return pedido.descripcion() + ", con carne";
    }
}
