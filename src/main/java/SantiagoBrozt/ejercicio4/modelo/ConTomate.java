package SantiagoBrozt.ejercicio4.modelo;

public class ConTomate implements Pedido {
    private static final int PRECIO = 100;
    private Pedido pedido;

    public ConTomate(Pedido pedido) {
        this.pedido = pedido;
    }

    @Override
    public int precio() {
        return PRECIO + pedido.precio();
    }

    @Override
    public String descripcion() {
        return pedido.descripcion() + ", con tomate";
    }
}
